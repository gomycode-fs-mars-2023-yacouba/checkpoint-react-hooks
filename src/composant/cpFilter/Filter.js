import React from 'react';
import './filter.css';

const Filter = ({ onFilterChange }) => {
  const handleTitleChange = (event) => {
    const title = event.target.value;
    onFilterChange({ title: title.toLowerCase() });
  };

  const handleRatingChange = (event) => {
    const rating = event.target.value;
    onFilterChange({ rating });
  };

  return (
    <div className="filter">
      <input type="text" placeholder="Filtrer par titre" onChange={handleTitleChange} />
      <input type="number" placeholder="Filtrer par note" min="1" max="10" onChange={handleRatingChange} />
    </div>
  );
};

export default Filter;
