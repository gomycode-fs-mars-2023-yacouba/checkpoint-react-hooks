import React from 'react';
import './MovieCard.css';

const MovieCard = ({ title, description, posterURL, rating }) => {
  return (
    <div className="movie-card">
      <img className="poster" src={posterURL} alt={title} />
      <h3 className="title">{title}</h3>
      <p className="description">{description}</p>
      <p className="rating">Note : {rating}</p>
    </div>
  );
};

export default MovieCard;
