import React, { useState } from 'react';

import MovieList from './composant/cpMovieList/MovieList';
import Filter from './composant/cpFilter/Filter';
import './App.css';

const App = () => {
  const [movies, setMovies] = useState([
    {
      title: 'Film 1',
      description: 'Description du film 1',
      posterURL: 'https://media.senscritique.com/media/000005676799/300/avengers.jpg',
      rating: 8.5
    },
    {
      title: 'Film 2',
      description: 'Description du film 2',
      posterURL: 'https://www.chroniquedisney.fr/imgFiliale/marvel/2021-venom-carnage-01-big.jpg',
      rating: 7.9
    },
    {
      title: 'Film 3',
      description: 'Description du film 2',
      posterURL: 'https://i.pinimg.com/originals/ae/e3/fa/aee3fac48852d4b9a84d6199a2a7681d.jpg',
      rating: 7.9
    },
    // Ajoutez d'autres films ici...
  ]);

  const [filter, setFilter] = useState({
    title: '',
    rating: ''
  });

  const handleFilterChange = (newFilter) => {
    setFilter({ ...filter, ...newFilter });
  };

  const filteredMovies = movies.filter((movie) => {
    const titleMatch = filter.title ? movie.title.toLowerCase().includes(filter.title.toLowerCase()) : true;
    const ratingMatch = filter.rating === '' || movie.rating >= parseFloat(filter.rating);
    return titleMatch && ratingMatch;
  });

  const handleAddMovie = (event) => {
    event.preventDefault();
    const title = event.target.elements.title.value;
    const description = event.target.elements.description.value;
    const posterURL = event.target.elements.posterURL.value;
    const rating = parseFloat(event.target.elements.rating.value);

    if (title && description && posterURL && rating) {
      const newMovie = {
        title,
        description,
        posterURL,
        rating
      };

      setMovies([...movies, newMovie]);

      event.target.reset();
    }
  };

  return (
    <div className="app">
      <h1>Ma collection de films</h1>
      <Filter onFilterChange={handleFilterChange} />
      <MovieList movies={filteredMovies} />
      <h2>Ajouter un nouveau film</h2>
      <form onSubmit={handleAddMovie}>
        <input type="text" name="title" placeholder="Titre" required />
        <input type="text" name="description" placeholder="Description" required />
        <input type="text" name="posterURL" placeholder="URL de l'affiche" required />
        <input type="number" name="rating" placeholder="Note" min="1" max="10" required />
        <button type="submit">Ajouter</button>
      </form>
    </div>
  );
};

export default App;
